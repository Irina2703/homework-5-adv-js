'use strict'

// Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.

// Технічні вимоги:
// При відкритті сторінки необхідно отримати з сервера список всіх користувачів
//  та загальний список публікацій.Для цього потрібно надіслати GET
//   запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts

// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації
//  на сторінці.
// Кожна публікація має бути відображена у вигляді картки
//     (приклад доданий в репозиторії групи, посилання на ДЗ вище),
//     та включати заголовок, текст, а також ім'я, прізвище та імейл користувача,
//      який її розмістив.
// На кожній картці повинна бути іконка або кнопка,
//     яка дозволить видалити цю картку зі сторінки.
//     При натисканні на неї необхідно надіслати DELETE запит на адресу
// https://ajax.test-danit.com/api/json/posts/${postId}.
//  Після отримання підтвердження із сервера(запит пройшов успішно),
//     картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених
// вище API можна знайти тут.
// Цей сервер є тестовим.Після перезавантаження сторінки всі зміни,
//     які надсилалися на сервер, не будуть там збережені.Це нормально,
//         все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів.
//  Для цього необхідно створити клас Card.При необхідності ви можете додавати також інші класи.


const postsInstant = axios.create({
    baseURL: 'https://ajax.test-danit.com/api/json'

});

let userResponse;
let postResponse;
const cardContainer = document.querySelector(".card-container");






class Card {
    constructor(id, name, email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    generateCardHtml() {
        return `
            <div  class=card id='${this.id}'>
                <h1>Name: ${this.name}</h1>
                <a href="">email: ${this.email}</a>   
                <button class="btn" data-id="${this.id}">DELETE</button><br>
            </div>`;
    }
}



postsInstant.get('/users').then((usersResponse) => {
    userResponse = usersResponse.data;
    console.log(userResponse);

    if (usersResponse.status === 200) {
        userResponse.forEach(({ name, email, id }) => {
            const newCard = new Card(id, name, email);
            cardContainer.insertAdjacentHTML('beforeend', newCard.generateCardHtml());
        });

        const btnDeletes = document.querySelectorAll(".btn");
        btnDeletes.forEach(btn => {
            btn.addEventListener('click', (event) => {
                const postId = event.target.getAttribute('data-id');

                axios.delete(`https://ajax.test-danit.com/api/json/posts/${postId}`)
                    .then(response => {
                        console.log(response);
                    })
                    .catch(error => {
                        console.error('Помилка видалення поста', error);
                    });
            });
        });
    } else {
        throw new Error('Помилка при отриманні даних користувачів');
    }

    postsInstant.get('/posts').then((postsResponse) => {
        postResponse = postsResponse.data;
        console.log(postResponse);

        if (postsResponse.status === 200) {
            postResponse.forEach(({ title, userId }) => {
                const elementId = document.getElementById(userId);
                if (elementId) {
                    elementId.insertAdjacentHTML('beforeend', `
                    <p>${title}</p>`)
                } else {
                    throw new Error('Помилка при отриманні даних постів');
                }
            });
        } else {
            throw new Error('Помилка при отриманні даних постів');
        }
    }).catch(error => console.error(error));
});
